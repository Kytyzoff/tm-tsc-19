package ru.tsc.borisyuk.tm.command.system;

import ru.tsc.borisyuk.tm.command.AbstractCommand;

public class CommandsShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String description() {
        return "Display list of commands...";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            String commandName = command.name();
            if (commandName != null && !commandName.isEmpty())
                System.out.println(commandName + ": " + command.description());
        }
    }

}
