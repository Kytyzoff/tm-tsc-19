package ru.tsc.borisyuk.tm.command.task;

import ru.tsc.borisyuk.tm.command.AbstractTaskCommand;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("[REMOVE TASK BY NAME]");
        serviceLocator.getTaskService().removeByName(userId, name);
        System.out.println("[OK]");
    }

}
