package ru.tsc.borisyuk.tm.exception.entity;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity not found.");
    }

}
