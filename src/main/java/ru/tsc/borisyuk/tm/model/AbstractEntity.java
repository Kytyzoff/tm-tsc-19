package ru.tsc.borisyuk.tm.model;

import java.util.UUID;

public abstract class AbstractEntity {

    protected String Id = UUID.randomUUID().toString();

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

}
