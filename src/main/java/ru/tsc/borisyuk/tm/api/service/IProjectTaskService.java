package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

    void removeAllTaskByProjectId(String projectId);

    Project removeById(String projectId);

    List<Task> findAllTaskByProjectId(String projectId);

}
